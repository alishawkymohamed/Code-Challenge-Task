const fs = require("fs");
const path = require("path");
const dicFile = path.join(__dirname, "Dictionary.json");

const createFileIfNotExistedPromise = new Promise((resolve, reject) => {
    if (!fs.existsSync(dicFile)) {
        fs.appendFileSync(dicFile, '{}');
        resolve();
    } else {
        resolve();
    }
});


const ReadFilePromise = new Promise((resolve, reject) => {
    createFileIfNotExistedPromise.then(() => {
        fs.readFile(dicFile, "utf8", (err, data) => {
            if (data) {
                resolve(JSON.parse(data));
            } else {
                reject(err);
            }
        });
    })
});

//Using Object to Store Key and Values.

class Dictionary {

    add(key, value) {
        ReadFilePromise.then(data => {
                let dicData = data;
                if (Object.keys(dicData).map((v) => v.toUpperCase()).includes(key.toUpperCase())) {
                    console.log("This is a duplicate key ...");
                    return;
                } else {
                    dicData[key] = value;
                    fs.writeFile(dicFile, JSON.stringify(dicData), (error) => {
                        if (error) throw err;
                    });
                }
            })
            .catch(error => {
                if (error) throw err;
            })
    }

    remove(key) {
        ReadFilePromise.then(data => {
                let dicData = data;
                if (!Object.keys(dicData).includes(key)) {
                    console.log(`The ${key} Key Is Not Existed ...`);
                    return;
                } else {
                    delete dicData[key];
                    fs.writeFile(dicFile, JSON.stringify(dicData), (error) => {
                        if (error) throw err;
                    });
                    console.log(`Key ${key} is now Removed !!`);
                }
            })
            .catch(error => {
                if (error) throw err;
            });
    }

    list() {
        ReadFilePromise.then(data => {
            let dicData = data;
            let keys = Object.keys(data);
            if (keys.length) {
                keys.forEach(key => {
                    console.log(`${key} ===> ${dicData[key]}`);
                });
            } else {
                throw Error("No Items");
            }
        })
    }

    get(key) {
        ReadFilePromise.then(data => {
                let dicData = data;
                if (!Object.keys(dicData).includes(key)) {
                    console.log("This Key Is Not Existed ...");
                    return;
                } else {
                    console.log(dicData[key]);
                }
            })
            .catch(error => {
                if (error) throw err;
            });
    }

    clear() {
        fs.writeFile(dicFile, JSON.stringify({}), (error) => {
            if (error) throw err;
        });
    }
}

module.exports = Dictionary;