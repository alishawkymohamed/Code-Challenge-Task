#! /usr/bin/env node

const Dictionary = require("./SimpleDictionary");

let command = process.argv[2];
let key = process.argv[3];
let value = process.argv[4];

const dic = new Dictionary();

if (command) {
    switch (command.toString().toUpperCase()) {
        case 'ADD':
            dic.add(key, value);
            break;

        case 'GET':
            dic.get(key);
            break;

        case 'LIST':
            dic.list();
            break;

        case 'REMOVE':
            dic.remove(key);
            break;

        case 'CLEAR':
            dic.clear();
            break;
        default:
            console.log(`
        .... Options To Be Used ....
            1- add key value
            2- list
            3- get key
            4- remove key
            5- clear
        `);
            break;
    }
} else {
    console.log('Please Specify The Operation You Want To Do ...');
}